// !Google Analytics

(function (i, s, o, g, r, a, m) {
	i["GoogleAnalyticsObject"] = r;
	(i[r] =
		i[r] ||
		function () {
			(i[r].q = i[r].q || []).push(arguments);
		}),
		(i[r].l = 1 * new Date());
	(a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m);
})(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");

ga("create", "UA-51855326-1", "postofficefarmnursery.com.au");
ga("send", "pageview");

(function ($) {
	function checkWidth() {
		// Desktop Menu Hover effect
		var windowSize = $(window).outerWidth();

		if (windowSize > 768) {
			$("li.menu-item-has-children")
				.mouseover(function () {
					$(this).children(".sub-menu").slideDown(300);
				})
				.mouseleave(function () {
					$(this).children(".sub-menu").slideUp(0);
				});
		}

		// Social icons move around
		var socialIcons = $(".top .social");

		if (windowSize > 768) {
			socialIcons.appendTo(".top");
		} else if (windowSize <= 768 && $(".top .social")) {
			socialIcons.prependTo(".mainNav");
		}
	}
	// Execute on load
	$(document).ready(checkWidth);
	// Bind event listener
	$(window).resize(checkWidth);

	$(document).ready(function () {
		$(".mobileToggle").click(function () {
			event.preventDefault();
			$(".mainNav .menu").slideToggle("fast").toggleClass("show");
		});
	});
})(jQuery);

(function ($) {
	function check_shipping() {
		var shipping = $(".shipping_method:checked").val();
		if (shipping == "legacy_local_pickup") {
			$(".delivery-options-message").hide();
			$("#deliveryoptions_field").hide();
			$("#order_comments_field").hide();
		} else {
			$(".delivery-options-message").show(300);
			$("#deliveryoptions_field").show(300);
			$("#order_comments_field").show(300);
		}
	}
	var refreshId = setInterval(check_shipping, 500);
})(jQuery);
