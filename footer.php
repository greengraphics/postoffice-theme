</div>


<div class="footerActions">
	<div class="container">

		<div class="footerActions__subscribe">

			<h4>Subscribe to our eNews</h4>
			<p>Subscribe to our newsletter and receive emails about all things Hellebores.</p>
			<a class="btn" href="http://eepurl.com/usXF9" title="Subscribe">Subscribe now</a>

		</div>

		<div class="footerActions__latest">
			<h4>Follow us</h4>
			<p>For all the latest news</p>

			<?php get_template_part( 'template-parts/social-icons' ); ?>

		</div>

	</div>
</div>
	
	
<footer class="footer">
	<div class="container">

		<div class="footer-credits">
			<?php echo wp_get_attachment_image( get_field( 'credit_image', 'options' ), 'medium' ); ?>

			<p><?php the_field( 'credit_text', 'options' ); ?></p>
		</div>

		<div class="footer-nav">

			<?php if ( has_nav_menu( 'footer-info' ) ) : ?>
			<nav class="info">
				<h4>Information</h4>
			
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer-info',
						'depth'          => 1,
						'container'      => '',
					)
				);
				?>
			</nav>
			<?php endif; ?>

		</div>
	
		<p class="alignleft">&copy; <?php echo date( 'Y' ); ?> Helleborus Rex Pty Ltd, trading as Post Office Farm Nursery | ABN: 30 099 408 803</p>
		<p class="alignright"><a href="https://greengraphics.com.au" >site by Greengraphics</a></p>
				
	</div>
</footer>


<?php wp_footer(); ?>
</body>
</html>
