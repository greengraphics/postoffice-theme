<?php get_header(); ?>
<main class="main">

	<article class="content">

	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();


			if ( is_shop_disabled() ) {
				the_field( 'shop_disabled_text' );
			} else {
				the_content();
			}

		endwhile;
	endif;

	if ( ! is_shop_disabled() || current_user_can( 'manage_options' ) ) :
		$taxonomy = 'product_cat';

		echo do_shortcode( '[product_category category="slug" per_page="100" columns="4" orderby="price" order="asc"]' );
		?>

		<h2>Helleborus x hybridus Selections</h2>

		<?php echo term_description( '17', $taxonomy ); ?>
		<?php echo do_shortcode( '[product_category category="helleborus-x-hybridus" per_page="100"]' ); ?>

		<h2>Caulescent species and hybrids</h2>

		<?php echo term_description( '16', $taxonomy ); ?>
		<?php echo do_shortcode( '[product_category category="caulescent" per_page="100"]' ); ?>

		<h2>Helleborus niger and its hybrids</h2>

		<?php echo term_description( '19', $taxonomy ); ?>
		<?php echo do_shortcode( '[product_category category="helleborous-niger-its-hybrids" per_page="100"]' ); ?>
	
	<?php endif; ?>

	</article>
	<?php get_sidebar(); ?>
</main>
<?php
get_footer();
