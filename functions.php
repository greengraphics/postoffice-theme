<?php

require_once get_theme_file_path( 'inc/custom-functions.php' );
require_once get_theme_file_path( 'inc/woocommerce-functions.php' );

function gg_setup() {
	load_theme_textdomain( 'postoffice', get_template_directory() . '/languages' );

	add_theme_support( 'title-tag' );

	register_nav_menus(
		array(
			'primary'     => __( 'Primary Navigation', 'postoffice' ),
			'footer-info' => __( 'Footer Info', 'postoffice' ),
		)
	);

	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	add_theme_support( 'woocommerce' );

	add_editor_style( array( 'css/editor-style.css' ) );

	if ( ! isset( $content_width ) ) {
		$content_width = 960;
	}

	add_image_size( 'banner', 1920, 350, true );
}
add_action( 'after_setup_theme', 'gg_setup' );


function gg_scripts() {
	wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,700;1,400;1,700&family=Vesper+Libre&display=swap' );
	wp_enqueue_style( 'themeStyle', get_stylesheet_uri() );

	wp_register_style( 'slider', get_theme_file_uri( 'css/slider.css' ), false, false, 'all' );

	wp_register_script( 'flexslider', get_template_directory_uri() . '/js/flexslider.js', array( 'jquery' ), '2.6.0', true );
	wp_register_script( 'slider', get_theme_file_uri( 'js/slider.js' ), array( 'jquery', 'flexslider' ), false, true );

	wp_enqueue_script( 'general', get_template_directory_uri() . '/js/general.js', array( 'jquery' ), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'gg_scripts' );


function mytheme_add_admin() {
	global $themename, $shortname, $options;

	if ( isset( $_REQUEST['saved'] ) ) {
		echo '<div id="message" class="updated fade"><p><strong>' . $themename . ' settings saved.</strong></p></div>';
	}
	if ( isset( $_REQUEST['reset'] ) ) {
		echo '<div id="message" class="updated fade"><p><strong>' . $themename . ' settings reset.</strong></p></div>';
	}
}
add_action( 'admin_menu', 'mytheme_add_admin' );


function gg_get_the_archive_title( $title ) {
	if ( is_post_type_archive() ) {
		$title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%2$s', 'postoffice' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives', 'postoffice' );
	}
	return $title;
}
add_filter( 'get_the_archive_title', 'gg_get_the_archive_title', 1 );



if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page(
		array(
			'page_title' => 'Theme General Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);
}


/**
 * A recursive strpos function.
 */
function strpos_recursive( $haystack, $needle, $offset = 0, &$results = array() ) {
	$offset = strpos( $haystack, $needle, $offset );
	if ( $offset === false ) {
		return $results;
	} else {
		$results[] = $offset;
		return strpos_recursive( $haystack, $needle, ( $offset + 1 ), $results );
	}
}


function is_shop_disabled() {
	$shop     = get_page_by_path( 'shop' );
	$disabled = get_field( 'disable_shop', $shop->ID );

	if ( $disabled ) {
		return true;
	}

	return false;
}

// Redirect customers away from woo.
function post_redirect() {
	if ( current_user_can( 'edit_posts' ) || ! is_shop_disabled() ) {
		return;
	}

	if ( is_woocommerce() || is_cart() || is_checkout() || is_product() ) {
		wp_redirect( home_url( '/shop/' ) );
		die;
	}
}
add_action( 'template_redirect', 'post_redirect' );



 // Trigger Holiday Mode

 // add_action ('init', 'njengah_woocommerce_holiday_mode');

  // Disable Cart, Checkout, Add Cart

function njengah_woocommerce_holiday_mode() {

	// remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

	// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

	// remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );

	// remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

	// add_action( 'woocommerce_before_main_content', 'njengah_wc_shop_disabled', 5 );

	// add_action( 'woocommerce_before_cart', 'njengah_wc_shop_disabled', 5 );

	// add_action( 'woocommerce_before_checkout_form', 'njengah_wc_shop_disabled', 5 );
}

 // Show Holiday Notice

function njengah_wc_shop_disabled() {

	  // wc_print_notice( 'Our Online Shop is Closed Today :) Please Come Back Tomorrow!', 'error');
}


require_once get_theme_file_path( 'inc/wc_email_confirm.php' );
