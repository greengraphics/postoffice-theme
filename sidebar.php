<?php 
    global $post;
    if(
        is_product() ||
        is_page()
    ) : 
?>

<aside class="sidebar">
    <?php
	$cartUrl = get_permalink(get_option('woocommerce_cart_page_id'));
	$acctUrl = get_permalink(get_option('woocommerce_myaccount_page_id'));

	if(
		is_woocommerce() ||
		$post->post_name == 'shop-open'
	) :

		if (! is_shop_disabled() ) :
			?>
			<a class='sqrBut' href='<?php echo esc_url(home_url()); ?>/shop' title='Shop'><span class='sqrBut__inner'>Back to Shop</span></a>
			<a class='sqrBut sqrBut--green' href='<?php echo esc_url($acctUrl); ?>' title='Cart'><span class='sqrBut__inner'>My Account</span></a>
			<a class="sqrBut sqrBut--brown" href="<?php echo esc_url(home_url('catalogue/gift-voucher')); ?>"><span class="sqrBut__inner">Gift Voucher</span></a>
			<?php
		endif; // ! is_shop_disabled()

	endif; ?>

</aside>

<?php endif;