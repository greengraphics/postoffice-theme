<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="viewport" content="wclassth=device-wclassth, initial-scale=1.0">
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="description" content="<?php bloginfo( 'description' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<header class="top">
	<h1 class="logoTitle"><a class="logo" href="<?php echo esc_url( home_url() ); ?>" title='<?php bloginfo(); ?>'><img alt="<?php bloginfo(); ?>" class="logo__img" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" /></a></h1>
	<h2 class="top__desc"><?php bloginfo( 'description' ); ?></h2>
	<?php get_template_part( 'template-parts/social-icons' ); ?>
</header>


<nav class="mainNav">
	<a class="mobileToggle" href="#" title="Menu"><?php require 'inc/icon--menu.svg'; ?></a>
<?php
	wp_nav_menu(
		array(
			'theme_location' => 'primary',
			'menu'           => 'primary',
			'container'      => '',
		)
	);
	?>
</nav>

<?php
	echo '<div class="banner">';
	$banner = get_field( 'page_banner' );

if ( is_single() ) {

	// do nothing

} elseif ( is_checkout() ) {

	// Do nothing
} elseif ( is_front_page() ) {
	// Load slider styles/scripts.
	wp_enqueue_style( 'slider' );
	wp_enqueue_script( 'slider' );

	// Slider on Home page
	echo '<div class="bannerSlider"><ul class="slides">';

	foreach ( $banner as $bannerImage ) {
		echo "<li class='banner__item'><img alt='" . $bannerImage['title'] . "' class='banner__img' src='" . $bannerImage['sizes']['banner'] . "'></li>";
	}

	echo '</ul></div>';
} elseif ( is_tax() ) {
	// Product Categories
	global $wp_query;
	$cat          = $wp_query->get_queried_object();
	$meta         = get_term_meta( $cat->term_id );
	$thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
	$banner       = wp_get_attachment_image_src( $thumbnail_id, 'banner' );

	echo "<img alt='" . get_the_archive_title() . "' class='banner__img' src='$banner[0]'>";

} else {
	// Check if banner is set
	if ( empty( $banner ) ) {
		$banner = get_field( 'page_banner', 177 );
	}
	// If banner is not set get random image from homepage
	if ( isset( $banner[0] ) ) {
		$count  = count( $banner );
		$banner = $banner[ rand( 0, $count - 1 ) ];
	}

	echo "<img alt='" . $banner['title'] . "' class='banner__img' src='" . $banner['sizes']['banner'] . "'>";
}

// Page Title
if ( is_front_page() ) {
	echo '<h1 class="page__title">Welcome</h1>';
} elseif ( is_checkout() ) {
// show nothing

} elseif ( is_archive() ) {
	echo '<h1 class="page__title">' . get_the_archive_title() . '</h1>';
} else {
	echo '<h1 class="page__title">' . get_the_title() . '</h1>';
}
	echo '</div>';
?>

<div class="container">
