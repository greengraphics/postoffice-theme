
<div class="social">

	<a class="social__icon" href="https://www.facebook.com/postofficefarmnursery" target="_blank" title="Follow us on Facebook">
		<?php include get_theme_file_path( 'inc/icon--facebook.svg'); ?>
	</a>
	
	<a class="social__icon" href="https://www.instagram.com/postofficefarmnursery/" target="_blank" title="Follow us on Instagram">
		<?php include get_theme_file_path( 'inc/icon--instagram.svg'); ?>
	</a>

</div>