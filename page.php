<?php get_header(); ?>
<main class="main">

	<article class="content">

	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();

			the_content();

		endwhile;
	endif;
	?>

	</article>
	<?php get_sidebar(); ?>
</main>
<?php
get_footer();
