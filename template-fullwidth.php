<?php 
/**
 * Template Name: Full Width
 */

get_header(); ?>
<main class="main">
    <article class="content content--full">

    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
    
    
    <?php
        // Action Buttons
        if(have_rows('action_buttons')) :
            $i=0;
            echo "<div class='actionButtons'>";
            while(have_rows('action_buttons')) : the_row();
                $i++;
                $label = get_sub_field('button_label');
                $link = get_sub_field('button_link');
                
                $classes = 'rndBut';
                if($i==2) $classes.=' rndBut--black';
                if($i==3) $classes.=' rndBut--green';
                
                echo "<a class='$classes' href='$link' title='$label'>$label</a>";

            endwhile;
            echo "</div>";
        endif;
    ?>
          
        <?php the_content(); ?>


    <?php endwhile; endif; ?>

    </article>
</main>
<?php get_footer(); ?>