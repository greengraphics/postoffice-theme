<?php


class WC_Email_Confirm {

	public function __construct() {
		add_filter('woocommerce_checkout_fields', [$this, 'checkout_fields'], 999, 1 );
		add_action('woocommerce_after_checkout_validation', [$this, 'validate_checkout_fields'], 10, 2);
		add_action('wp_footer', [$this, 'validate_checkout_fields_js']);
	}

	public function checkout_fields($fields) {
		if ( is_user_logged_in() ) {
			return $fields;
		}

		// Email confirm field.
		$fields['billing']['billing_email_confirm'] = $fields['billing']['billing_email'];
		$fields['billing']['billing_email_confirm']['label'] = __('Confirm email address');
		$fields['billing']['billing_email_confirm']['autocomplete'] = 'disabled';

		return $fields;
	}

	public function validate_checkout_fields( $data, $errors ) {
		$e = $_POST["billing_email"];
		$ec = $_POST["billing_email_confirm"];

		if ( isset($ec) && $e !== $ec ) {
			$errors->add( 'billing_email_confirm',  __( "<strong>Confirm Email address</strong> does not match <strong>Email address</strong>." ), 'error' );
		}
	}

	function validate_checkout_fields_js() {
		if ( is_user_logged_in() || ! is_checkout() ) {
			return;
		}
	?>
	<script>
		jQuery(function($){
			$( 'body' ).on( 'blur change', '#billing_email_confirm', function(){
				const wrapper = $(this).closest( '.form-row' );
				const email = $('body #billing_email');

				if( $(this).val() == email.val() ) { // check if contains numbers
					wrapper.removeClass( 'woocommerce-invalid' );
					wrapper.addClass( 'woocommerce-validated' ); // success
				} else {
					wrapper.removeClass( 'woocommerce-validated' );
					wrapper.addClass( 'woocommerce-invalid' ); // error
				}
			});
		});
	</script>
	<?php
	}
}

new WC_Email_Confirm();
