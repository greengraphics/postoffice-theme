<?php
if ( ! function_exists( 'is_shop' ) ) {
	return;
}

/**
 * Remove Related Products
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
 * Change Main content wrapper
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
add_action( 'woocommerce_before_main_content', 'open_main_wrapper', 10 );
add_action( 'woocommerce_before_main_content', 'open_content_wrapper', 15 );
add_action( 'woocommerce_after_main_content', 'close_content_wrapper', 10 );
add_action( 'woocommerce_sidebar', 'close_main_wrapper', 15 );

/**
 * Remove Woo Sidebar
 */
if (
	   is_shop()        // Returns true when viewing the product type archive (shop).
	|| is_cart()        // Returns true when viewing the cart page.
	|| is_checkout()    // Returns true when viewing the checkout page.
	) {
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
}


// Cart Menu Item
add_shortcode( 'woocommerce_cart_icon', 'woo_cart_icon' );
function woo_cart_icon() {
	ob_start();

		$cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
		$cart_url   = wc_get_cart_url();  // Set variable for Cart URL

		echo '<li class="menu-item"><a class="menu-item cart-contents" href="' . $cart_url . '" title="Cart">';

	if ( $cart_count > 0 ) {

		echo '<span class="cart-contents-count">' . $cart_count . '</span>';

	}

		echo '</a></li>';

	return ob_get_clean();

}


add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_icon_count' );
function woo_cart_icon_count( $fragments ) {

	ob_start();

	$cart_count = WC()->cart->cart_contents_count;
	$cart_url   = wc_get_cart_url();

	echo '<a class="cart-contents menu-item" href="' . $cart_url . '" title="View Cart">';

	if ( $cart_count > 0 ) {

		echo '<span class="cart-contents-count">' . $cart_count . '</span>';

	}
	echo '</a>';

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}


add_filter( 'wp_nav_menu_items', 'woo_cart_icon_menu', 10, 2 );
function woo_cart_icon_menu( $menu, $args ) {

	if ( $args->theme_location == 'primary' ) { // 'primary' is my menu ID
		$cart = do_shortcode( '[woocommerce_cart_icon]' );
		return $menu . $cart;
	}

	return $menu;
}


/**
 * Remove Tabs from single Product page
 *
 * @param arr $tabs
 * @return arr
 */
function po_remove_product_tabs( $tabs ) {
	unset( $tabs['reviews'] );                // Remove the reviews tab
	unset( $tabs['additional_information'] ); // Remove the additional information tab

	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'po_remove_product_tabs', 10, 1 );

/**
 * Add the payment method to New Order Emails
 *
 * @param obj  $order Woocommerce order object.
 * @param bool $is_admin_email Is admin conditional.
 * @return void
 */
function add_payment_method_to_admin_new_order( $order, $is_admin_email ) {
	if ( $is_admin_email ) {
		echo '<h2><strong>Payment Method:</strong> ' . $order->payment_method_title . '</h2>';
	}
}
add_action( 'woocommerce_email_before_order_table', 'add_payment_method_to_admin_new_order', 15, 2 );

/**
 * Remove City and Postcode from shipping calculator
 */
add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
add_filter( 'woocommerce_shipping_calculator_enable_postcode', '__return_false' );

/**
 * Order products by SKU
 *
 * @param arr $args
 * @return arr
 */
function po_order_catalog( $args ) {
	$args['meta_key'] = '_sku';
	$args['orderby']  = 'meta_value';
	$args['order']    = 'asc';
	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'po_order_catalog' );


/* Archive after description button */
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
function po_archive_description() {
	printf(
		'<a class="btn btn-lrg" href="https://www.postofficefarmnursery.com.au/how-we-do-things/">How we do things <span>(read before ordering)</span></a>'
	);
}
add_action( 'woocommerce_archive_description', 'po_archive_description' );

/**
 * Customise the Checkout fields
 *
 * @param [type] $fields
 * @return void
 */
function po_checkout_fields( $fields ) {

	unset( $fields['billing']['billing_address_2'] );
	unset( $fields['shipping']['shipping_address_2'] );

	// Order fields.
	$fields['order']['order_comments']['placeholder'] = 'Please provide brief instructions here if you would like your order held till a specific date or if there are dates when you will be away';
	$fields['order']['order_comments']['label']       = __( 'Information on delivery timing', 'woocommerce' );

	return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'po_checkout_fields', 999, 1 );

function po_default_address_fields( $fields ) {
	$fields['address_1']['label']       = __( 'Street Address / PO Box', 'woocommerce' );
	$fields['address_1']['placeholder'] = 'Street Address/ PO Box';
	$fields['address_1']['autocomplete'] = 'disabled';

	$fields['city']['label']       = __( 'Suburb / Town', 'woocommerce' );
	$fields['city']['placeholder'] = 'Suburb / Town';
	$fields['city']['autocomplete'] = 'disabled';

	$fields['state']['label'] = __( 'State', 'woocommerce' );
	$fields['state']['class'] = array( 'form-row-first', 'address-field' );

	$fields['postcode']['label'] = __( 'Postcode', 'woocommerce' );
	$fields['postcode']['class'] = array( 'form-row-last', 'address-field' );

	$fields['country']['value'] = 'AU';

	$fields['company']['label']       = __( 'Company / Business', 'woocommerce' );
	$fields['company']['placeholder'] = 'Company / Business';

	return $fields;
}
add_filter( 'woocommerce_default_address_fields', 'po_default_address_fields', 999, 1 );

function po_gettext( $translated ) {
	if ( is_checkout() && 'Suburb' === $translated ) {
		return 'Suburb / Town';
	}

	return $translated;
}
add_filter( 'gettext', 'po_gettext' );


function po_after_order_notes() {
	echo '';
}
add_action( 'woocommerce_after_order_notes', 'po_after_order_notes', 10 );


function wps_add_select_checkout_field( $checkout ) {

	$shipping = WC()->cart->needs_shipping();

	// Hide delivery option if there is nothing to ship.
	if ( ! $shipping ) {
		return;
	}

	$select_delivery = woocommerce_form_field(
		'deliveryoptions',
		array(
			'type'        => 'select',
			'class'       => array( 'form-row-wide' ),
			'input_class' => array( 'select-delivery', 'select2' ),
			'required'    => true,
			'return'      => true,
			'label'       => __( 'Delivery instructions', 'postoffice' ),
			'options'     => array(
				'blank'      => __( 'Select a delivery option', 'postoffice' ),
				'frontdoor'  => __( 'At the front door', 'postoffice' ),
				'frontporch' => __( 'On the front porch', 'postoffice' ),
				'safeplace'  => __( 'Any safe place on my premises', 'postoffice' ),
				'carport'    => __( 'Under carport / verandah', 'postoffice' ),
				'letterbox'  => __( 'In my letterbox (if size allows)', 'postoffice' ),
				'sidegate'   => __( 'By the side gate', 'postoffice' ),
				'signature'  => __( 'Require my signature', 'postoffice' ),
			),
		),
		$checkout->get_value( 'deliveryoptions' )
	);

	$select_delivery = str_replace(
		'</label>',
		sprintf(
			'</label>
			<span class="delivery-options-message">If you are not home when parcel is delivered, Australia Post will attempt to leave parcel in a safe place if one is available.
			If no safe place is available then a card will be left and the parcel will be taken to nearby Post Office for you to collect.
			Select from list below to assist with delivery.
			If you do not wish for parcel to be left if no one is home please select ‘require my signature’.<br><br>
			Choose Pickup from Nursery below if you can collect from our nursery for free shipping!</span>'
		),
		$select_delivery
	);

	printf(
		'%s
		<script>jQuery(document).ready(function() {
			jQuery(".select-delivery").select2({
				width: "100%%"
			});
		});</script>',
		$select_delivery
	);
}
add_action( 'woocommerce_before_order_notes', 'wps_add_select_checkout_field', 20 );


// * Process the checkout
add_action( 'woocommerce_checkout_process', 'wps_select_checkout_field_process' );
function wps_select_checkout_field_process() {
	global $woocommerce;
	// Check if set, if its not set add an error.
	if ( $_POST['deliveryoptions'] == 'blank' && strpos_recursive( 'pickup', $_POST['shipping_method'] ) === false ) {
		 wc_add_notice( '<strong>Please select a delivery option</strong>', 'error' );
	}
}

// * Update the order meta with field value
add_action( 'woocommerce_checkout_update_order_meta', 'wps_select_checkout_field_update_order_meta' );
function wps_select_checkout_field_update_order_meta( $order_id ) {
	if ( $_POST['deliveryoptions'] ) {
		update_post_meta( $order_id, 'deliveryoptions', esc_attr( $_POST['deliveryoptions'] ) );
	}
}


// * Display field value on the order edition page
add_action( 'woocommerce_admin_order_data_after_billing_address', 'wps_select_checkout_field_display_admin_order_meta', 10, 1 );
function wps_select_checkout_field_display_admin_order_meta( $order ) {
	echo '<p><strong>' . __( 'Delivery option', 'postoffice' ) . ':</strong> ' . get_post_meta( $order->id, 'deliveryoptions', true ) . '</p>';
}
// * Add selection field value to emails
function wps_select_order_meta_keys( $keys ) {
	$keys['Delivery option'] = 'deliveryoptions';
	return $keys;
}
add_filter( 'woocommerce_email_order_meta_keys', 'wps_select_order_meta_keys' );


/**
 * Change the button text for products that are out of stock
 *
 * @param str $text The string of text.
 * @param obj $product The product object.
 * @return str
 */
function po_add_to_cart_text( $text, $product ) {
	if ( 'outofstock' === $product->get_stock_status() ) {
		$text = __( 'Out of Stock', 'postoffice' );
	}
	return $text;
}
add_filter( 'woocommerce_product_add_to_cart_text', 'po_add_to_cart_text', 10, 2 );


function po_replace_paypal_icon() {
	return 'https://postofficefarmnursery.com.au/assets/paypaltrans.png';
}
add_filter( 'woocommerce_paypal_icon', 'po_replace_paypal_icon' );


add_action(
	'after_woocommerce_pay',
	function() {
		echo "<div class='after-pay-notice'>
		<p>
		Please note,<br>
		if paying by Credit Card: The CVN (Card Verification Number) <br>
		is a 3 - 4 digit number printed on the back of your card.</p>
	</div>";
	}
);

add_filter(
	'woocommerce_credit_card_form_fields',
	function( $fields ) {
		echo $fields;
	},
	10,
	1
);


// PICKLIST EDITS.
add_filter( 'wf_pklist_alter_print_css', 'po_pklist_custom_css', 100, 3 );
function po_pklist_custom_css( $css, $template_type, $template_for_pdf ) {
	$css = file_get_contents( get_theme_file_path('wf-woocommerce-packing-list/print.css'));

	return $css;
}

// Add Pagedjs to pklist for layout and page numbers.
function po_pklist_template_html( $html ) {

	// Cancel body onload print event & load pagedjs then print.
	$html = "<script>
		
		document.body.onload = null;

		window.PagedConfig = {
			after: () => window.print()
		}
  	</script>
	<script src='https://unpkg.com/pagedjs/dist/paged.polyfill.js'></script>" . $html;

	return $html;
}
add_filter( 'wt_pklist_alter_final_order_template_html', 'po_pklist_template_html' );


add_filter( 'wf_pklist_alter_order_grouping_row_text', 'po_pklist_group_row_text', 10, 3 );
function po_pklist_group_row_text( $order_info_arr, $order, $template_type ) {
	if ( $template_type == 'picklist' ) {
		// Remove invoice number.
		unset( $order_info_arr[1] );

		$name = $order->get_shipping_first_name() ?: $order->get_billing_first_name();
		$last =  $order->get_shipping_last_name() ?: $order->get_billing_last_name();

		// Order Number.
		array_splice(
			$order_info_arr,
			0,
			0,
			"$name $last"
		);

		// Delivery Options.
		$order_info_arr[] = 'Delivery Options: ' . $order->get_meta( 'deliveryoptions' );

		// Order Items.
		$items   = $order->get_items();
		$itm_qty = 0;
		foreach ( $items as $item ) {
			$product  = wc_get_product( $item->get_product_id() );
			$attr     = (int) $product->get_attribute( 'pa_mix' ) ?: 1;
			$itm_qty += ( $item->get_quantity() * $attr );
		}
		$order_info_arr[] = "Total Order Items: $itm_qty";
	}
	return $order_info_arr;
}


add_filter( 'wf_pklist_alter_order_grouping_row_text', 'po_pklist_group_row_text_wraps', 12, 3 );
function po_pklist_group_row_text_wraps( $order_info_arr, $order, $template_type ) {

	$new_array = array();
	foreach ( $order_info_arr as $i => $order_arr ) {
		$new_array[] = "<span class='pklist-row-text col-$i'>$order_arr</span>";
	}

	return $new_array;
}

add_filter( 'wf_pklist_package_product_tbody_html', 'po_pklist_alter_order_html', 10 );
function po_pklist_alter_order_html( $html ) {

	// Find the order number and name.
	$rows   = explode( '</tr>', $html );
	$orders = array();
	foreach ( $rows as $key => $row ) {
		if ( strpos( $row, 'wfte_product_table_order_row' ) !== false ) {
			preg_match( '/pklist-row-text col-0["\']>(.+?)<\/span>/', $row, $name );
			preg_match( '/pklist-row-text col-1["\']>Order number: (.+?)<\/span>/', $row, $id );

			$name = $name[1];
			$id   = $id[1];
		}
		$orders[ "$name $id" ][] = $row;
	}

	// Sort the orders by name.
	ksort( $orders, SORT_NATURAL | SORT_FLAG_CASE );

	// Wrap the orders in tbodys to avoid print page breaks.
	$newhtml = '';
	$ordercount = count($orders);
	$orderi = 1;
	foreach ( $orders as $order ) {
		$order_html = implode( '</tr>', $order );
		$pre = '<tbody class="wfte_product_table_body wfte_table_body_color">';
		$post = '</tbody>';

		if ($orderi === 1 ) {
			$pre = '';
		}

		if ( $orderi === $ordercount ) {
			$post = '';
		}

		$newhtml .= $pre .$order_html . $post;
		$orderi++;
	}

	return $newhtml;
}


// Reorder pklist Orders.
add_action( 'init', 'po_pklist_package_order', 9 );
function po_pklist_package_order() {
	$query = $_GET;
	if ( ! isset( $query['print_packinglist'] ) || isset( $query['sorted'] ) ) {
		return;
	}

	$order_ids = explode( ',', $query['post'] );
	$orders    = array();
	foreach ( $order_ids as $order_id ) {
		$order = wc_get_order( $order_id );
		$name = $order->get_shipping_first_name() ?: $order->get_billing_first_name();
		$last =  $order->get_shipping_last_name() ?: $order->get_billing_last_name();
		$key = "$name $last $order_id";

		$orders[ $key ] = $order_id;
	}

	ksort( $orders, SORT_NATURAL | SORT_FLAG_CASE );
	$query['post']   = implode( ',', $orders );
	$query['sorted'] = true;
	$query           = build_query( $query );
	$url             = get_site_url() . $_SERVER['REQUEST_URI'];
	$url             = str_replace( $_SERVER['QUERY_STRING'], $query, $url );

	header( "Location: $url" );
	die;
}


// ShipStation meta.
function po_shipstation_custom_field_3( $key ) {
	return 'deliveryoptions';
}
add_filter( 'woocommerce_shipstation_export_custom_field_3', 'po_shipstation_custom_field_3' );
