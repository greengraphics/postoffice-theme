<?php

function open_main_wrapper() {
    echo "<main class='main'>";
}


function open_content_wrapper() {
    
    if(is_shop() || is_tax() || is_cart() || is_checkout()) {
        echo "<article class='content content--full'>";
    } else {
        echo "<article class='content'>";
    }
}


function close_content_wrapper() {
    echo "</article>";
}


function close_main_wrapper() {
    echo "</main>";
}


function no_posts() {
    
    open_main_wrapper();
    
        open_content_wrapper();
        
            echo "<h2 align='center'>Not Found</h2><p align='center'>Sorry, but you are looking for something that isn't here.</p>";
            
        close_content_wrapper();
    
    close_main_wrapper();
    
}
