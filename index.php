<?php get_header(); ?>


<?php if(have_posts()) : while(have_posts()) : the_post(); ?>


    <?php open_main_wrapper(); ?>
    

        <?php open_content_wrapper(); ?>    
    
                    
            <?php the_content(); ?>

       
        <?php close_content_wrapper(); ?>    


        <?php get_sidebar(); ?>  
        
        
    <?php close_main_wrapper(); ?>
       
        	
<?php endwhile; else : ?>
        
        
    <?php no_posts(); ?>
        
        
<?php endif; ?>


<?php get_footer(); ?>